<?php
	Yii::import('webroot.plugins.CrmWebService.services.ServicesAbstract');
	Yii::import('webroot.plugins.CrmWebService.services.ServicesInterface');

    /**
     * @author RABOTOVAO Hery
     * @copyright 2014
     * @version 0.1.3
     * This plugin was created to let LimeSurvey send a survey entry to a third party webservice.
     * Actually running with GetResponce webservice, you can easly add multiple webservices to LimeSurvey as this plugin is extensible.
     * When active this plugin let to add and configure a list of webservice per survey.
     * A complete survey will send a lead to all configured webservice. 
     */
    class CrmWebService extends PluginBase {

        protected $storage = 'DbStorage';    
        static protected $description = 'Core: Send a survey to some crm\'s webservice';
        static protected $name = 'CRM Webservice';
        
        
        protected $webServices = array(); //Don't fill this array manually. Please add new service at loadServices().
        
        public $email_fiedname = 'cws_email';

       
        /**
         * Load all webservice as a ServicesAbstract object 
         */
        public function loadWebServices(){
        	
        	//Load classes and push new services object
        	Yii::import('webroot.plugins.CrmWebService.services.GetResponse.GetResponseAbstract');
        	$this->webServices [] = new GetResponseAbstract();
        	
        	Yii::import('webroot.plugins.CrmWebService.services.Aweber.AweberAbstract');
        	$this->webServices [] = new AweberAbstract();
        	
        	//Add new services here below
        }
        
        public function __construct(PluginManager $manager, $id) {
            parent::__construct($manager, $id);
			
			//Load all Services
            $this->loadWebServices();

            //Init event
            $this->subscribe('afterSurveyComplete','sendSurvey');
            $this->subscribe('beforeSurveySettings');
            $this->subscribe('newSurveySettings');
            $this->subscribe('beforeSurveyPage');
 
        }
 
        
        /**
         * Fired on beforeSurveySettings event
         * Init all settings field for a survey
         */
        public function beforeSurveySettings(){
        	
        	$event      = $this->getEvent();
        	
        	$settings = array();
        	$settings['cws_code_campaign'] = array(
        			'type' => 'string',
        			'label' => 'Code Campagne <br>Pas d\'espaces et de caractères spéciaux autre que _',
        			'default'=> false
        	);
        	foreach($this->webServices as $service)
        		$settings = array_merge($settings, $service->getSettings());
        	
        	foreach( array_keys($settings) as $key)
        		$settings[$key]['current'] = $this->get($key, 'Survey', $event->get('survey'));
        	
        	$event->set("surveysettings.{$this->id}", array(
                'name' => get_class($this),
                'settings' => $settings
                )
            );
        }
        
        /**
         * Fired on newSurveySettings event
         * Save all settings field for a survey
         */
        public function newSurveySettings()
        {
        	$event = $this->getEvent();
        	foreach ($event->get('settings') as $name => $value)
        	{
        		$this->set($name, $value, 'Survey', $event->get('survey'));
        	}
        }
        
        /**
         * Fired on beforeSurveyPage event
         * Add email field at the end of the survey
         */
        public function beforeSurveyPage(){
        	
        	$assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets');
        	Yii::app()->clientScript->registerCssFile($assetUrl . '/cws_styles.css');
        	Yii::app()->clientScript->registerScriptFile($assetUrl . '/jquery.validate.min.js',CClientScript::POS_HEAD);
        	Yii::app()->clientScript->registerScriptFile($assetUrl . '/additional-methods.min.js',CClientScript::POS_HEAD);
        	Yii::app()->clientScript->registerScriptFile($assetUrl . '/cws_main.js',CClientScript::POS_END);
        	if(isset($_GET[$this->email_fiedname]) && $_GET[$this->email_fiedname])
        		Yii::app()->clientScript->registerScript("cws_script", "var cws_email_input_type = 'hidden'; var cws_email = '".$_GET[$this->email_fiedname]."';", CClientScript::POS_BEGIN);
        	elseif(isset($_POST[$this->email_fiedname]) && $_POST[$this->email_fiedname])
	        	Yii::app()->clientScript->registerScript("cws_script", "var cws_email_input_type = 'hidden'; var cws_email = '".$_POST[$this->email_fiedname]."';", CClientScript::POS_BEGIN);
	        else
        		Yii::app()->clientScript->registerScript("cws_script", "var cws_email_input_type = 'text';", CClientScript::POS_BEGIN);
        }

        /**
        * Fired on afterSurveyComplete event
        * Send data to the registred web service
        */
        public function sendSurvey()
        {
            $event      = $this->getEvent();
        	
            $surveyId   = $event->get('surveyId');
        	$surveys	= getSurveyInfo($surveyId);
        	$surveys_name = $this->get('cws_code_campaign', 'Survey', $surveyId) ? $this->get('cws_code_campaign', 'Survey', $surveyId) : $surveys['surveyls_title'];
        	
        	// Check user_email
        	// Notes: 'cws_email' is not the same key as $this->email_fieldname property.  Don't change this one below.
        	
        	if(isset($_POST['cws_email']) && $_POST['cws_email']){
	        	$user_email = $_POST['cws_email'];
	        	
	        	//GET All response
	        	$responseId = $event->get('responseId');
	        	$response   = $this->formatResponse($this->pluginManager->getAPI()->getResponse($surveyId, $responseId));
				
	        	// Send Request to availables services
	        	foreach($this->webServices as $service){
	        		//Init Service custom setup
	        		foreach(array_keys($service->getSettings()) as $key)
	        			$service->addSettingValue(str_replace($service->getUniqueName().'__','',$key), $this->get($key, 'Survey', $surveyId));
	        		//Send request if the service is active
					
	        		if($this->get($service->getUniqueName(), 'Survey', $surveyId))
	        			$service->sendRequest($this->slugify($surveys_name), $user_email, $service->processResponse($response));
	        	}
        	}
        }
        
        /**
         * Process response before sending response to the web services
         * @param unknown $data
         * @return multitype:
         */
        public function formatResponse($data){
        	
        	$data = array_filter($data );
        	//Remove non-useful fields
        	$fields_to_unset = array('id','token','submitdate','lastpage','startlanguage','ipaddr','refurl','startdate','datestamp');
        	foreach(array_keys($data) as $key){
        		if(in_array($key,$fields_to_unset))
        			unset($data[$key]);
        	}
        	/*echo '<pre>';
        	var_dump($data);
        	echo '</pre>';
        	exit();*/
        	return $data;
        }
        
        /**
         * Slugify text with '_' character not '-' character
         * @param string $text
         * @return string|NULL
         */
        static public function slugify($text)
        {
        	// replace non letter or digits by _
        	$text = preg_replace('~[^\\pL\d]+~u', '_', $text);
        
        	// trim
        	$text = trim($text, '_');
        
        	// transliterate
        	if (function_exists('iconv'))
        	{
        		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        	}
        
        	// lowercase
        	$text = strtolower($text);
        
        	// remove unwanted characters
        	$text = preg_replace('~[^-\w]+~', '', $text);
        
        	if (empty($text))
        	{
        		return NULL;
        	}
        
        	return $text;
        }
        
    }
