<?php
class  AweberAbstract extends ServicesAbstract{
	
	protected $uniqueName ="WS_Aweber";
	
	protected $name = "AWeber";
	
	protected $label = "Activer l'API AWeber";
	
	/*
	 * Add the custom fields needed for the authentification workflow (api_key, username, password, ...)
	*/
	protected $setting_fields = array(
			'consummer_key',
			'consummer_secret',
			'access_key',
			'access_secret',
			'listname'
	);
	
	/**
	 * Return settings for Lime Survey
	 * Overide the ServicesAbstract::getSettings by adding some specifics fields
	 * @return array
	 */
	public function getSettings(){
		$settings = array();
		//A service need an checkbox for enable or for disable a service
		$settings[$this->getUniqueName()] = array(
				'type' => 'select',
				'label' => $this->getLabel(),
				'options' => array(
						0 => 'Désactiver',
						1 => 'Activer'
				)
		);
		
		$settings[$this->uniqueName.'__message'] = array(
				'type' => 'info',
				'content'=> 'Pour récupérer les clés nécessaire à l\'activation de Aweber, veuillez cliquer le bouton ci-dessous.'
		);
		
		$settings[$this->uniqueName.'__connexion'] = array(
				'type' => 'link',
				'label' => 'Récupérer les clés d\'activation',
				'link'=> Yii::app()->baseUrl.'/aweber-redirect-uri.php'
		);
		
		//Custom setting field
		foreach($this->setting_fields as $key=>$value){
			$style = array();
			if(sizeof($this->setting_fields)==($key+1))
				$style = array('margin-bottom'=>'20px');

			$settings[$this->uniqueName.'__'.$value] = array(
					'label'=>strtoupper($value),
					'type'=>'string',
					'style'=>$style
			);
		}
		
		$settings[$this->uniqueName.'__listname'] = array(
				'type' => 'string',
				'label' => 'AWeber LIST_NAME <br> Ajouter le nom de votre liste AWeber'
		);
		
		return $settings;
	}
	
	/** 
	 * Implement the service workflow on this method for Sending Request to the webservice
	 * First we need to check/add the campaign to the api.  Then we check/add the contact to he api.
	 * All other field from the survey will be save as user's custom fields 
	 * @param string $campaign, string $email, array $data
	 * @return void
	 */
	public function SendRequest($campaign, $user_email, $data=array()) {
		//Require AWeber API PHP Wrapper
		Yii::import('webroot.plugins.CrmWebService.services.Aweber.lib.AWeberPHPWrapper.aweber_api.curl_object',true);
		Yii::import('webroot.plugins.CrmWebService.services.Aweber.lib.AWeberPHPWrapper.aweber_api.curl_response',true);
		Yii::import('webroot.plugins.CrmWebService.services.Aweber.lib.AWeberPHPWrapper.aweber_api.aweber',true);
		
		//Init
		$consumerKey = $this->getSettingValue('consummer_key');
		$consumerSecret = $this->getSettingValue('consummer_secret');
		$accessKey = $this->getSettingValue('access_key');
		$accessSecret = $this->getSettingValue('access_secret');
		$listName = $this->getSettingValue('listname');

		$application = new AWeberAPI($consumerKey, $consumerSecret);
		$account = $application->getAccount($accessKey, $accessSecret);
		
		//Get list
		$list = null;
		
		try {
			$foundLists = $account->lists->find(array('name' => $listName));
			
			if(!sizeof($foundLists)){
				throw new Exception("Erreur : La liste '$listName' n'a pas &eacute;t&eacute; trouver");
			}

			if(isset($foundLists[0]) && $foundLists[0]->name==$listName)
				$list = $foundLists[0];
			else
				throw new Exception("Erreur : La liste '$listName' n'a pas &eacute;t&eacute; trouver");

		}catch(Exception $e) {
				throw new Exception("<b>Aweber Error: </b> ".$e->getMessage());
		}
		
		
		//Set mandatory contact's infos
		$contact_infos = array(
				'email'		 => $user_email,
		);
		
		//Add Custom field
		# lets create some custom fields on your list!
		$custom_fields = $list->custom_fields;
		if( is_array($data) && $data ){
			
				# setup your custom fields
				foreach($data as $key=>$val){
					try {
						$custom_fields->create(array('name' => $key,'is_subscriber_updateable'=>true));
					}catch(AWeberAPIException $e) {

					}
					$contact_infos['custom_fields'][$key] = $val;
				}
					
		}
		
		
		try {
			//Get Subscribers
			$subscribers_list = $list->subscribers->find(array('email'=>$user_email));
			
			//Update Subscriber
			if(sizeof($subscribers_list)){
				$subscriber = $subscribers_list[0];
				//Update custom fields if needed
				foreach($data as $key=>$val){
					$subscriber->custom_fields[$key] = $val;
					$subscriber->save();
				}
			}else{
				//Add Subscribers
				$subscriber = $list->subscribers->create($contact_infos);
			}
			
		}catch(Exception $e) {
			throw new Exception("<b>Aweber Error: </b> ".$e->getMessage());
		}
		
	}
}