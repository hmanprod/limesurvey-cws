<?php
class  GetResponseAbstract extends ServicesAbstract{
	
	protected $uniqueName ="WS_GetResponse";
	
	protected $name = "Get Response";
	
	protected $label = "Activer l'API Get Response";
	
	/*
	 * Add the custom fields needed for the authentification workflow (api_key, username, password, ...)
	*/
	protected $setting_fields = array('api_key');
	
	
	/** 
	 * Implement the service workflow on this method for Sending Request to the webservice
	 * First we need to check/add the campaign to the api.  Then we check/add the contact to he api.
	 * All other field from the survey will be save as user's custom fields 
	 * @param string $campaign, string $email, array $data
	 * @return void
	 */
	public function SendRequest($campaign, $user_email, $data=array()) {
		//Require GetResponse jsonRPCClient class
		Yii::import('webroot.plugins.CrmWebService.services.GetResponse.lib.jsonRPCClientGR');
		
		//Init
		$api_key = $this->getSettingValue('api_key');
		$api_url = 'http://api2.getresponse.com';
		$client = new jsonRPCClientGR($api_url);
		//$client->setDebug(true);
		
		//Get campaign		
		$result = $client->get_campaigns(
				$api_key,
				array (
						'name' => array ( 'EQUALS' => $campaign ) //Only Slugified text using '_' not '-'
				)
		);

		//Add new campaign
		if(!sizeof($result)){
			$result = $client->add_campaign(
					$api_key,
					array (
							'name' => $campaign, //Only Slugified text using '_' not '-'
							'description' => $campaign,
							'from_field' => "BmD9", // Pick one from the result of this method : $client->get_account_from_fields($api_key);
							'reply_to_field' => "BmD9", 
							'confirmation_subject' => "V1", // Pick one from the result of this method : $client->get_confirmation_subjects($api_key);
							'confirmation_body' => "V1", // Pick one from the result of this method : $client->get_confirmation_bodies($api_key);
							"language_code" => "FR"
					)
			);
		}
		if(isset($result['CAMPAIGN_ID']) && $result['CAMPAIGN_ID'])
			$CAMPAIGN_ID = $result['CAMPAIGN_ID'];
		else{
			$CAMPAIGN_IDS = array_keys($result);
			$CAMPAIGN_ID = $CAMPAIGN_IDS[0];
		}

		if(!$CAMPAIGN_ID)return;

		//Set mandatory contact's infos
		$contact_infos = array(
				'campaign'  => $CAMPAIGN_ID,
				'email'		 => $user_email,
		);
		
		//Add Custom field
		if( is_array($data) && $data ){
			foreach($data as $key=>$val){
				$contact_infos['customs'][] = array('name'=>$key, 'content'=>$val);
			}
		}
		
		//Get user if exists
		$contacts = $client->get_contacts(
							$api_key,
							array(
								'campaigns'=>array($CAMPAIGN_ID),
								'email'=>array('EQUALS'=>$user_email)						
							)
					);

		if(sizeof($contacts)){
			//Update user custom field
			$contactIDs = array_keys($contacts);
			try {
				$result = $client->set_contact_customs(
							$api_key,
							array(
								'contact'=>$contactIDs[0],
								'customs'=>$contact_infos['customs']
							)
					);
			}catch (Exception $e) {
				/*echo '<pre>';
				 var_dump($e);
				echo '</pre>';
				exit();*/
				//throw new Exception("<b>GetResponse Error: </b> ".$e->getMessage());
			}		
		}else{
			//Add contact
			try {
				$result = $client->add_contact(
						$api_key,
						$contact_infos
				);
			}catch (Exception $e) {
				/*echo '<pre>';
	        	var_dump($e);
	        	echo '</pre>';
	        	exit();*/
				//throw new Exception("<b>GetResponse Error: </b> ".$e->getMessage());
			}
		}
	}	
}