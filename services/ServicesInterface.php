<?php
interface ServicesInterface{
	
	/*
	 * Return the unique name of this service
	 */
	public function getName();
	
	/*
	 * Return the name of this service
	 */
	public function getUniqueName();
	
	/*
	 * Return the setting's label of this service
	 */
	public function getLabel();
	
	/*
	 * Return custom setting field
	 */
	public function getSettings();
	
	/*
	 * Add a setting value. Must be done before exec a sendRequest.
	 */
	public function addSettingValue($key, $value);
	
	public function getSettingValue($key);
	
	/**
	 * Implement the service workflow on this method for Sending Request to the webservice
	 * @param string $campaign, string $email, array $data
	 * @return void
	 */
	public function SendRequest($campaign, $user_email, $data=array());
	
	/**
	 * You can overide this method to format the response data
	 * before sending response to the webservice
	 * @param array $data
	 * @return array
	 */
	public function processResponse($data);
	
}