<?php
abstract class ServicesAbstract implements ServicesInterface{
	
	protected $uniqueName = "";
	
	protected $name = "";
	
	protected $label = "";
	
	/*
	 * Add the custom fields needed for the authentification workflow (api_key, username, password, ...)
	*/
	protected $setting_fields = array();
	
	/*
	 * Here we'll store all custom fields with their values
	 */
	protected $settings = array();
	
	/**
	 * Return the unique name of this service
	 * @return string
	 */
	public function getUniqueName(){
		return $this->uniqueName;
	}
	
	/**
	 * Return the name of this service
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}
	
	/**
	 * Return the setting's label of this service
	 * @return string
	 */
	public function getLabel(){
		return $this->label;
	}
	
	/**
	 * Return settings for Lime Survey
	 * @return array
	 */
	public function getSettings(){
		$settings = array();
		//A service need an checkbox for enable or for disable a service
		$settings[$this->getUniqueName()] = array(
				'type' => 'select',
				'label' => $this->getLabel(),
				'options' => array(
						0 => 'Désactiver',
						1 => 'Activer'
				)
		);
		//Custom setting field
		foreach($this->setting_fields as $key=>$value){
			$style = array();
			if(sizeof($this->setting_fields)==($key+1))
				$style = array('margin-bottom'=>'20px');
			
			$settings[$this->uniqueName.'__'.$value] = array(
				'label'=>strtoupper($value),
				'type'=>'string',
				'style'=>$style
			);
		}
		return $settings;
	}
	
	public function addSettingValue($key, $value){
		if(in_array($key, $this->setting_fields))
			$this->settings[$key] = $value;
	}
	
	public function getSettingValue($key){
		if(isset($this->settings[$key]))
			return $this->settings[$key];
		return NULL;
	}
	
	public function getSettingValues(){
			return $this->settings;
	}
	
	/**
	 * You can overide this method to format the response data
	 * before sending response to the webservice
	 * @param array $data
	 * @return array
	 */
	public function processResponse($data){
		return $data;
	}
}