$(function(){
	if(typeof(cws_email_input_type) != 'undefined' && cws_email_input_type == 'text' && !$("#movenextbtn").length)
		$("#lastgroup").before(
				'<div class="text-short mandatory">'+
				'<div class="question">'+
				'<div class="questiontext">'+
				'<span class="asterisk">* </span>Votre email<br>'+
				'</div>'+
				'<div class="answers">'+
				'<table><tbody><tr><td><p class="question answer-item text-item ">'+
				'<input type="email" value="" name="cws_email" size="50" class="text empty" required />'+
				'</p></td></tr></tbody></table>'+
				'</div></div></div>'
				);
	if(typeof(cws_email_input_type) != 'undefined' && cws_email_input_type=='hidden' && typeof(cws_email)!= 'undefined'){
		$("#lastgroup").before('<input type="hidden" value="'+cws_email+'" name="cws_email" />');
	}
	
	$( "#limesurvey" ).validate();
});